package com.than.swingtutorial;

import javax.swing.JButton;
import javax.swing.JFrame;

class MyApp {
    JFrame frame;
    JButton button;
    public MyApp(){
        frame = new JFrame();
        frame.setTitle("First JFrame");
        frame.setSize(400,500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        
        button = new JButton("Click");
        button.setBounds(130,100,100,40);
        frame.setLayout(null);
        frame.add(button);
    }
}

public class Simple {
    public static void main(String[] args) {
        MyApp app = new MyApp();
    }
}
